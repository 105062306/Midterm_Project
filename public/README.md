# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer
* Firebase app link: https://ss-mid-pro.firebaseapp.com

## Topic
* [Topic Name]
* Key functions (add/delete)
    1. [user page]: Your account name, email, and profile photo would show on the navbar, and you can click the 'my post' button to see 
    the list of all your posts.
    2. [post page]: After clcking the 'read me' button, you are allowed access your entire article and the comment_section so as to   leave and read comments.
    3. [post list page]: You can enter the recent 100 posts after clicking the 'recent page'.
    4. [leave comment]: After entering the post page, you can read and leave comments.
* Other functions (add/delete)
    1. [delete comment]: You can delete your own comment by clicking the 'X' icon of the comment.
    2. [like the post]: You can like te post if you click the star icon of the post, and you can see the amount of stars at the title   bar of the post.
    3. [show comment count]: The comment-count would show at the title bar of the post.
    4. [delete post]: You can delete your own post by clicking the 'delete post' button of the post.
    5. [show top posts]: You can order your post by stars if you click the 'Your top posts' button.

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description

* Basic components
    1. User can login through google account or sign in with email. If there's no such account, user can create one with email.
    2. User can visit the website through Gitlab page. (https://105062306.gitlab.io/Midterm_Project)
    3. The website would show the login page if not logged in, so can't read and write to the database.
    And I regulate the database rule to not allowed unauthorized users to access.
    4. As the device-width is as wide as desktop, three colums of post element would show at the post-list page, and as the device-width is as wide as tablet, only one post element would show and the font-size would be smaller.
    
* Advance components
    1. User can sign up with their google account, and their user name and profile picture would also be shown.
    2. As user update a new post, they would get a google notification that they have deployed a post.
    3. Elements at login page and posts would fade in or fly in through css animation.


## Security Report (Optional)
1.  When user write anything in the forum, the website would read only as text instead of html, so we couldn't write any code to attack the website.
2. User is not allowed to press F12 key so as to protect the code.

## References 
1. firebase quickstart (https://github.com/firebase/quickstart-js)
2. w3-school css (https://www.w3schools.com/w3css/4/w3.css)
3. google notification
4. Material design lite (https://getmdl.io/)