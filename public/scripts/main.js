/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';


// Shortcuts to DOM Elements.
var messageForm = document.getElementById('message-form');
var messageInput = document.getElementById('new-post-message');
var titleInput = document.getElementById('new-post-title');
var btnGoogle = document.getElementById('sign-in-button-google');
var btnSignIn = document.getElementById('sign-in-button-email');
var btnSignUp = document.getElementById('sign-up-button-email');
var txtEmail = document.getElementById('inputEmail');
var txtPassword = document.getElementById('inputPassword');
var signOutButton = document.getElementById('sign-out-button');
var splashPage = document.getElementById('page-splash');
var addPost = document.getElementById('add-post');
var addButton = document.getElementById('add');
var recentPostsSection = document.getElementById('recent-posts-list');
var userPostsSection = document.getElementById('user-posts-list');
var topUserPostsSection = document.getElementById('top-user-posts-list');
var recentMenuButton = document.getElementById('menu-recent');
var myPostsMenuButton = document.getElementById('menu-my-posts');
var myTopPostsMenuButton = document.getElementById('menu-my-top-posts');
var complete_post = document.getElementById('complete_post');
var listeningFirebaseRefs = [];
var updates = {};



function scrollFunction() {
    console.log('scroll!!');
    //alert('scroll');
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("top_btn").style.display = "block";
    } else {
        document.getElementById("top_btn").style.display = "none";
    }
}

document.addEventListener('DOMContentLoaded', function () {
  if (!Notification) {
      alert('Desktop notifications not available in your browser. Try Chromium.'); 
      return;
  } 
  if (Notification.permission !== "granted")
      Notification.requestPermission();
});


/**
 * Saves a new post to the Firebase DB.
 */
// [START write_fan_out]
function writeNewPost(uid, username, picture, title, body, email) {
  // A post entry.
  var postData = {
    author: username,
    uid: uid,
    body: body,
    title: title,
    starCount: 0,
    authorPic: picture,
    authorEmail: email
  };

  // Get a key for a new Post.
  var newPostKey = firebase.database().ref().child('posts').push().key;

  // Write the new post's data simultaneously in the posts list and the user's post list.
  
  updates['/posts/' + newPostKey] = postData;
  updates['/user-posts/' + uid + '/' + newPostKey] = postData;
  if (Notification.permission == "granted"){
    var notification = new Notification('Notification', {
        icon :picture || './silhouette.jpg',
        body: (username || email)+'  updates a new post!'
    });
    
  }
  

  return firebase.database().ref().update(updates);
}
// [END write_fan_out]

/**
 * Star/unstar post.
 */
// [START post_stars_transaction]
function toggleStar(postRef, uid) {
  postRef.transaction(function(post) {
    if (post) {
      if (post.stars && post.stars[uid]) {
        post.starCount--;
        post.stars[uid] = null;
      } else {
        post.starCount++;
        if (!post.stars) {
          post.stars = {};
        }
        post.stars[uid] = true;
      }
    }
    return post;
  });
}
// [END post_stars_transaction]

/**
 * Creates a post element.
 */

function createPostElement(postId, title, text, author, authorId, authorPic, authorEmail) {
  var uid = firebase.auth().currentUser.uid;

  var html =
      '<div class="post post-' + postId + ' mdl-cell mdl-cell--12-col ' +
                  'mdl-cell--6-col-tablet mdl-cell--4-col-desktop mdl-grid mdl-grid--no-spacing top_class w3-animate-opacity" >' +
        '<div class="mdl-card mdl-shadow--2dp">' +
          '<div class="star" style="float:right">' +
            '<div class="not-starred material-icons">star_border</div>' +
            '<div class="starred material-icons ">star</div>' +
            '<div class="star-count">0</div>' +
            '<div class="comm material-icons">comment</div>' +
            '<div class="comment-count">0</div>' +
          '</div>' +    
          '<div class="mdl-card__title mdl-color--red-300 mdl-color-text--white">' +
            '<h4 class="mdl-card__title-text"></h4>' +            
          '</div>' +          
          '<div class="header">' +
            '<div>' +
              '<div class="avatar"></div>' +
              '<div class="username mdl-color-text--black"></div>' +
            '</div>' +
          '</div>' +                         
          '<div class="text" style="overflow:ellipsis"></div>' +
          '<span class="read_more" ><a>read more...</a></span>' + 
          '<button class="delete_post" style="float: right;">delete post</button>' + 
          '<div class="comment_section">' +
            '<br><br>'+
            '<div class="comments-container" style="overflow:ellipsis"></div>' +
          '</div>' +
            '<form class="add-comment" action="#"  >' +
              '<div class="mdl-textfield mdl-js-textfield">' +
                '<input class="mdl-textfield__input new-comment" type="text" >' +
                '<label class="mdl-textfield__label" >Comment...</label>' +
              '</div>' +
            '</form>' +          
        '</div>' +
      '</div>';

  // Create the DOM element from the HTML.
  var div = document.createElement('div');
  div.innerHTML = html;
  var postElement = div.firstChild;
  if (componentHandler) {
    componentHandler.upgradeElements(postElement.getElementsByClassName('mdl-textfield')[0]);
  }

  var addCommentForm = postElement.getElementsByClassName('add-comment')[0];
  var commentInput = postElement.getElementsByClassName('new-comment')[0];
  var star = postElement.getElementsByClassName('starred')[0];
  var unStar = postElement.getElementsByClassName('not-starred')[0];
  var read_more = postElement.getElementsByClassName('read_more')[0];
  var delete_post = postElement.getElementsByClassName('delete_post')[0];

  // Set values.
  postElement.getElementsByClassName('text')[0].innerText = text;
  postElement.getElementsByClassName('mdl-card__title-text')[0].innerText = title;
  postElement.getElementsByClassName('username')[0].innerText = author || authorEmail || 'Anonymous';
  postElement.getElementsByClassName('avatar')[0].style.backgroundImage = 'url("' +
      (authorPic || './silhouette.jpg') + '")';

  
  // Listen for comments.
  // [START child_event_listener_recycler]
  var commentsRef = firebase.database().ref('post-comments/' + postId);
  commentsRef.on('child_added', function(data) {
    addCommentElement(postElement, postId, data.key, data.val().text, data.val().author||data.val().authorEmail);
    updateCommentCount(postElement, 1);
  });

  commentsRef.on('child_changed', function(data) {
    setCommentValues(postElement, data.key, data.val().text, data.val().author||data.val().authorEmail);
  });

  commentsRef.on('child_removed', function(data) {
    deleteComment(postElement, data.key);
    updateCommentCount(postElement, -1);
  });
  // [END child_event_listener_recycler]

  // Listen for likes counts.
  // [START post_value_event_listener]
  var starCountRef = firebase.database().ref('posts/' + postId + '/starCount');
  starCountRef.on('value', function(snapshot) {
    updateStarCount(postElement, snapshot.val());
  });
  // [END post_value_event_listener]

  // Listen for the starred status.
  var starredStatusRef = firebase.database().ref('posts/' + postId + '/stars/' + uid);
  starredStatusRef.on('value', function(snapshot) {
    updateStarredByCurrentUser(postElement, snapshot.val());
  });

  // Keep track of all Firebase reference on which we are listening.
  listeningFirebaseRefs.push(commentsRef);
  listeningFirebaseRefs.push(starCountRef);
  listeningFirebaseRefs.push(starredStatusRef);

  // Create new comment.
  addCommentForm.onsubmit = function(e) {
    e.preventDefault();
    createNewComment(postId, firebase.auth().currentUser.displayName || firebase.auth().currentUser.email, uid, commentInput.value);
    commentInput.value = '';
    commentInput.parentElement.MaterialTextfield.boundUpdateClassesHandler();
  };

  // Bind starring action.
  var onStarClicked = function() {
    var globalPostRef = firebase.database().ref('/posts/' + postId);
    var userPostRef = firebase.database().ref('/user-posts/' + authorId + '/' + postId);
    toggleStar(globalPostRef, uid);
    toggleStar(userPostRef, uid);
  };

  //bind read nmmore action.
  read_more.onclick = function(){
    let curPostsRef = firebase.database().ref('posts').orderByChild('title').equalTo(title);
    function fetch() {
      curPostsRef.on('child_added', function(data) {
        let author = data.val().author ||data.val().authorEmail ||'Anonymous';
        let containerElement = complete_post.getElementsByClassName('posts-container')[0];
        containerElement.insertBefore(
          createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().authorPic, data.val().authorEmail),
          containerElement.firstChild);
          var postElement = containerElement.getElementsByClassName('post-' + data.key)[0];
          postElement.getElementsByClassName('comment_section')[0].style.display='block';
          postElement.getElementsByClassName('add-comment')[0].style.display='block';
          postElement.getElementsByClassName('read_more')[0].style.display='none';
          postElement.getElementsByClassName('mdl-card__title-text')[0].innerText = data.val().title;
          postElement.getElementsByClassName('text')[0].innerText = data.val().body;
          postElement.classList.remove('mdl-cell--4-col-desktop');
          postElement.classList.add('mdl-cell--12-col-desktop');          
          if(postElement.getElementsByClassName('username')[0].innerText===firebase.auth().currentUser.displayName || postElement.getElementsByClassName('username')[0].innerTex===firebase.auth().currentUser.email)
            postElement.getElementsByClassName('delete_post')[0].style.display='inline';
          else  
            postElement.getElementsByClassName('delete_post')[0].style.display='none';
      });
      curPostsRef.on('child_changed', function(data) {
        let containerElement = complete_post.getElementsByClassName('posts-container')[0];
        let postElement = containerElement.getElementsByClassName('post-' + data.key)[0];
        postElement.getElementsByClassName('mdl-card__title-text')[0].innerText = data.val().title;
        postElement.getElementsByClassName('username')[0].innerText = data.val().author||data.val().authorEmail;
        postElement.getElementsByClassName('text')[0].innerText = data.val().body;
        postElement.getElementsByClassName('star-count')[0].innerText = data.val().starCount;
        postElement.getElementsByClassName('comment_section')[0].style.display='block';
        postElement.getElementsByClassName('add-comment')[0].style.display='block';
        postElement.getElementsByClassName('read_more')[0].style.display='none';        
        postElement.classList.remove('mdl-cell--4-col-desktop');
        postElement.classList.add('mdl-cell--12-col-desktop');
        if(postElement.getElementsByClassName('username')[0].innerText===firebase.auth().currentUser.displayName || postElement.getElementsByClassName('username')[0].innerText===firebase.auth().currentUser.email)
          postElement.getElementsByClassName('delete_post')[0].style.display='inline-block';
        else  
          postElement.getElementsByClassName('delete_post')[0].style.display='none'; 
      });
      curPostsRef.on('child_removed', function(data) {
        let containerElement = complete_post.getElementsByClassName('posts-container')[0];
        let post = containerElement.getElementsByClassName('post-' + data.key)[0];
        post.parentElement.removeChild(post);
      });
      
    }
    // Keep track of all Firebase refs we are listening to.
    listeningFirebaseRefs.push(curPostsRef);    
    complete_post.getElementsByClassName('posts-container')[0].innerHTML = '';
    showSection(complete_post);
    fetch();
  }



  delete_post.onclick = function(){
  
    firebase.database().ref('user-posts/'+ authorId).child(postId).remove();
    firebase.database().ref('post-comments/').child(postId).remove();
    firebase.database().ref('posts/').child(postId).remove();
    updates['/posts/' + postId] = null;
    updates['/user-posts/' + authorId + '/' + postId] = null;
    firebase.database().ref().update(updates);
    ;
  }

  unStar.onclick = onStarClicked;
  star.onclick = onStarClicked;
  
  return postElement;
}

/**
 * Writes a new comment for the given post.
 */
function createNewComment(postId, username, uid, text) {
  firebase.database().ref('post-comments/' + postId).push({
    text: text,
    author: username,
    uid: uid
  });
}

/**
 * Updates the starred status of the post.
 */
function updateStarredByCurrentUser(postElement, starred) {
  if (starred) {
    postElement.getElementsByClassName('starred')[0].style.display = 'inline';
    postElement.getElementsByClassName('not-starred')[0].style.display = 'none';
  } else {
    postElement.getElementsByClassName('starred')[0].style.display = 'none';
    postElement.getElementsByClassName('not-starred')[0].style.display = 'inline';
  }
}

/**
 * Updates the number of stars displayed for a post.
 */
function updateStarCount(postElement, nbStart) {
  postElement.getElementsByClassName('star-count')[0].innerText = nbStart;
}


/**
 * Updates the number of comments displayed for a post.
 */
function updateCommentCount(postElement, nbComment) {
  var cur_comments = postElement.getElementsByClassName('comment-count')[0].innerText;
  var cur_comments_number = parseInt(cur_comments,10);
  cur_comments_number=cur_comments_number+nbComment;
  postElement.getElementsByClassName('comment-count')[0].innerText=cur_comments_number.toString();
}


/**
 * Creates a comment element and adds it to the given postElement.
 */
function addCommentElement(postElement,postId, id, text, author) {
  var comment = document.createElement('div');
  comment.classList.add('comment-' + id);
  comment.innerHTML = '<span class="username"></span><span class="delete_comment  material-icons">clear</span><span class="comment" style="overflow:ellipsis"></span>';
  comment.getElementsByClassName('comment')[0].innerText = text;
  comment.getElementsByClassName('username')[0].innerText = (author || 'Anonymous' )+' :';

  if(author===firebase.auth().currentUser.displayName || author===firebase.auth().currentUser.email)
    comment.getElementsByClassName('delete_comment')[0].style.display='inline';
  else  
    comment.getElementsByClassName('delete_comment')[0].style.display='none';

  var commentsContainer = postElement.getElementsByClassName('comments-container')[0];
  commentsContainer.appendChild(comment);

  comment.getElementsByClassName('delete_comment')[0].onclick = function(){firebase.database().ref('post-comments/'+postId).child(id).remove();};

}

/**
 * Sets the comment's values in the given postElement.
 */
function setCommentValues(postElement, id, text, author) {
  var comment = postElement.getElementsByClassName('comment-' + id)[0];
  comment.getElementsByClassName('comment')[0].innerText = text;
  comment.getElementsByClassName('fp-username')[0].innerText = author;
}

/**
 * Deletes the comment of the given ID in the given postElement.
 */
function deleteComment(postElement, id) {
  var comment = postElement.getElementsByClassName('comment-' + id)[0];
  comment.parentElement.removeChild(comment);
}

/**
 * Starts listening for new posts and populates posts lists.
 */
function startDatabaseQueries() {
  // [START my_top_posts_query]
  var myUserId = firebase.auth().currentUser.uid;
  var topUserPostsRef = firebase.database().ref('user-posts/' + myUserId).orderByChild('starCount');
  // [END my_top_posts_query]
  // [START recent_posts_query]
  var recentPostsRef = firebase.database().ref('posts').limitToLast(100);
  // [END recent_posts_query]
  var userPostsRef = firebase.database().ref('user-posts/' + myUserId);


  var fetchPosts = function(postsRef, sectionElement) {
    postsRef.on('child_added', function(data) {
      let author = data.val().author ||data.val().authorEmail|| 'Anonymous';
      let containerElement = sectionElement.getElementsByClassName('posts-container')[0];
      containerElement.insertBefore(
        createPostElement(data.key,data.val().title, data.val().body, author, data.val().uid, data.val().authorPic,data.val().authorEmail),
        containerElement.firstChild);
      let postElement = containerElement.getElementsByClassName('post-' + data.key)[0];
      postElement.getElementsByClassName('comment_section')[0].style.display='none';
      postElement.getElementsByClassName('add-comment')[0].style.display='none';
      postElement.getElementsByClassName('read_more')[0].style.display='inline';
      postElement.getElementsByClassName('delete_post')[0].style.display='none';
      if(data.val().title.length>8)
        postElement.getElementsByClassName('mdl-card__title-text')[0].innerText = data.val().title.substr(0,8)+'...';
      if(data.val().body.length>15)
        postElement.getElementsByClassName('text')[0].innerText = data.val().body.substr(0,15)+'...';
    });
    postsRef.on('child_changed', function(data) {
      let containerElement = sectionElement.getElementsByClassName('posts-container')[0];
      let postElement = containerElement.getElementsByClassName('post-' + data.key)[0];
      postElement.getElementsByClassName('username')[0].innerText = data.val().author||data.val().authorEmail;
      postElement.getElementsByClassName('star-count')[0].innerText = data.val().starCount;
      postElement.getElementsByClassName('comment_section')[0].style.display='none';
      postElement.getElementsByClassName('add-comment')[0].style.display='none';
      postElement.getElementsByClassName('read_more')[0].style.display='inline';
      postElement.getElementsByClassName('delete_post')[0].style.display='none';
      if(data.val().title.length>8)
        postElement.getElementsByClassName('mdl-card__title-text')[0].innerText = data.val().title.substr(0,8)+'...';
      if(data.val().body.length>15)
        postElement.getElementsByClassName('text')[0].innerText = data.val().body.substr(0,15)+'...';      
    });
    postsRef.on('child_removed', function(data) {
      let containerElement = sectionElement.getElementsByClassName('posts-container')[0];
      let post = containerElement.getElementsByClassName('post-' + data.key)[0];
      post.parentElement.removeChild(post);
    });
  };

  // Fetching and displaying all posts of each sections.
  fetchPosts(topUserPostsRef, topUserPostsSection);
  fetchPosts(recentPostsRef, recentPostsSection);
  fetchPosts(userPostsRef, userPostsSection);
  
  // Keep track of all Firebase refs we are listening to.
  listeningFirebaseRefs.push(topUserPostsRef);
  listeningFirebaseRefs.push(recentPostsRef);
  listeningFirebaseRefs.push(userPostsRef);
 
}

/**
 * Writes the user's data to the database.
 */
// [START basic_write]
function writeUserData(userId, name, email, imageUrl) {
  firebase.database().ref('users/' + userId).set({
    username: name || email,
    email: email,
    profile_picture : imageUrl
  });
}
// [END basic_write]

/**
 * Cleanups the UI and removes all Firebase listeners.
 */
function cleanupUi() {
  // Remove all previously displayed posts.
  topUserPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';
  recentPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';
  userPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';
  complete_post.getElementsByClassName('posts-container')[0].innerHTML = '';

  // Stop all currently listening Firebase listeners.
  listeningFirebaseRefs.forEach(function(ref) {
    ref.off();
  });
  listeningFirebaseRefs = [];
}

/**
 * The ID of the currently signed-in User. We keep track of this to detect Auth state change events that are just
 * programmatic token refresh but not a User status change.
 */
var currentUID;


/**
 * Triggers every time there is a change in the Firebase auth state (i.e. user signed-in or user signed out).
 */
function onAuthStateChanged(user) {
  // We ignore token refresh events.
  if (user && currentUID === user.uid) {
    return;
  }

  cleanupUi();

  if (user) {
    currentUID = user.uid;
    splashPage.style.display = 'none';
    writeUserData(user.uid, user.displayName, user.email, user.photoURL);   
   
    document.getElementById('user_info').innerHTML='<p>User name : '+(user.displayName || user.email)+'  <img src="'+ (user.photoURL || './silhouette.jpg')+'" width="25" style="border-radius: 25px;"></img></p><p>User email : '+user.email+'</p>';
    //document.getElementsByClassName('avatar')[0].innerHTML='<img src="'+user.photoURL+'" width="32"></img>';
    startDatabaseQueries();
  } else {
    // Set currentUID to null.
    currentUID = null;
    // Display the splash page where you can sign-in.
    splashPage.style.display = '';
  }
}

/**
 * Creates a new post for the current user.
 */
function newPostForCurrentUser(title, text) {
  // [START single_value_read]
  var userId = firebase.auth().currentUser.uid;
  return firebase.database().ref('/users/' + userId).once('value').then(function(snapshot) {
    var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
    // [START_EXCLUDE]
    return writeNewPost(firebase.auth().currentUser.uid, username,
      firebase.auth().currentUser.photoURL,
      title, text,firebase.auth().currentUser.email);
    // [END_EXCLUDE]
  });
  // [END single_value_read]
}

/**
 * Displays the given section element and changes styling of the given button.
 */
function showSection(sectionElement, buttonElement) {
  recentPostsSection.style.display = 'none';
  userPostsSection.style.display = 'none';
  topUserPostsSection.style.display = 'none';
  addPost.style.display = 'none';
  complete_post.style.display = 'none';
  recentMenuButton.classList.remove('is-active');
  myPostsMenuButton.classList.remove('is-active');
  myTopPostsMenuButton.classList.remove('is-active');

  if (sectionElement) {
    sectionElement.style.display = 'block';
  }
  if (buttonElement) {
    buttonElement.classList.add('is-active');
  }
}

// Bindings on load.
window.addEventListener('load', function() {
  // Bind Sign in button.
  btnGoogle.addEventListener('click', function() {
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider);
    
  });
  btnSignIn.addEventListener('click', function () {
    var email = txtEmail.value;
    var password = txtPassword.value;
    firebase.auth().signInWithEmailAndPassword(email, password)
      .catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        alert(errorMessage);
        txtEmail.value = "";
        txtPassword.value = "";
      });
  });

  btnSignUp.addEventListener('click', function () {
    var email = txtEmail.value;
    var password = txtPassword.value;
    firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(function () {
        txtEmail.value = "";
        txtPassword.value = "";
      })
      .catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        alert(errorMessage);
        txtEmail.value = "";
        txtPassword.value = "";
      });
  });  

  window.onscroll = function() {scrollFunction()};

  // Bind Sign out button.
  signOutButton.addEventListener('click', function() {
        firebase.auth().signOut();
  });

  // Listen for auth state changes
  firebase.auth().onAuthStateChanged(onAuthStateChanged);

  // Saves message on form submit.
  messageForm.onsubmit = function(e) {
    e.preventDefault();
    var text = messageInput.value;
    var title = titleInput.value;
    if (text && title) {
      newPostForCurrentUser(title, text).then(function() {
        myPostsMenuButton.click();
      });
      messageInput.value = '';
      titleInput.value = '';
    }
    
  }
  // Bind menu buttons.
  recentMenuButton.onclick = function() {
    showSection(recentPostsSection, recentMenuButton);
  };
  myPostsMenuButton.onclick = function() {
    showSection(userPostsSection, myPostsMenuButton);
  };
  myTopPostsMenuButton.onclick = function() {
    showSection(topUserPostsSection, myTopPostsMenuButton);
  };
  addButton.onclick = function() {
    showSection(addPost);
    messageInput.value = '';
    titleInput.value = '';
  };
  recentMenuButton.onclick();
}, false);


document.onkeydown = function (event) {
  event = (event || window.event);
  if (event.keyCode == 123) {
      //alert('No F-keys');
      return false;
  }
}
